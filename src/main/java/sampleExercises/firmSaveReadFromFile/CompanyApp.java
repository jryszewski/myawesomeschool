package sampleExercises.firmSaveReadFromFile;

import java.io.*;
import java.util.Scanner;

public class CompanyApp {
    //- klasa, która pozwala wczytywać dane od użytkownika i zapisać je na dysku lub odczytać dane z dysku i wyświetlić je na ekranie.
    private static final String fileName = "C:\\Users\\jrysz\\Desktop\\employers.txt";
    private static final int read_from_user = 1;
    private static final int read_from_file = 2;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Wprowadzanie danych pracowników -" + read_from_user);
        System.out.println("Wczytaj i wyświetl dane z pliku - " + read_from_file);

        int option = scanner.nextInt();
        scanner.nextLine();
        if(option==read_from_user){
            Company company = createCompany();
            writeFile(company);
        } else if(option==read_from_file){
            Company company = null;
            try {
                company = readFile();
            } catch (IOException | ClassNotFoundException e) {
                System.out.println("Błąd odczytu danych");
            }
            System.out.println(company);
        }
        scanner.close();
    }

    private static Company createCompany() {
        Company company = new Company();
        for (int i = 0; i < 3; i++) {

            System.out.println("Wprowadź imie:");
            String firstName = scanner.nextLine();
            System.out.println("Wprowadź nazwisko:");
            String lastName = scanner.nextLine();
            System.out.println("Wprowadź wypłatę:");
            double salary = scanner.nextDouble();
            scanner.nextLine();
            company.add(new Employee(firstName, lastName, salary));
        }
        return company;
    }


    private static Company readFile() throws IOException, ClassNotFoundException {
        try (
                FileInputStream fileInputStream = new FileInputStream(fileName);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {
            return (Company) objectInputStream.readObject();
        }
    }
    private static void writeFile(Company company){
        try(
                FileOutputStream fileOutputStream = new FileOutputStream(fileName);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                ){
            objectOutputStream.writeObject(company);
            System.out.println("zapisano do pliku");

        }catch (IOException e){
            System.out.println("Błąd zapisu pliku");
        }
    }

}

