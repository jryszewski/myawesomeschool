package sampleExercises.firmSaveReadFromFile;

import java.io.Serializable;

public class Company implements Serializable {

    Employee[] employees = new Employee[3];
    private int index =0;

    public void add(Employee employee){
        employees[index]= employee;
        index++;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Employee emp:employees
             ) {
            builder.append(emp.toString());
            builder.append("\n");

        }
        return builder.toString();
    }
}
