package sampleExercises.firmSaveReadFromFile;

public class Employee extends Person  {
    private double salary;

    public Employee(String firstName, String lastName, double salary) {
        super(firstName, lastName);
        this.salary = salary;
    }

    public double getsalary() {
        return salary;
    }

    public void setsalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return super.toString()+ ", wypłata: "+ salary+"zł";
    }
}
