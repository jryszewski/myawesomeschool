package sampleExercises;



public class PairTest {
    public static void main(String[] args) {



        Pair<String, Integer> pair = new Pair<>("word1",1);
        Pair<Double, String> pair1 = new Pair<>(2.4,"word2");
        Pair<Character, Integer> pair2 = new Pair<>('a',2);

         showInformation(pair1);
         showInformation(pair2);
    }

    public static  <T,V>void showInformation(Pair<T,V> pair){
        System.out.println(pair.toString());
    }
}
