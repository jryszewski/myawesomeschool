package sampleExercises;

public class Cart {


    private Product[] products = new Product[10];
    private int productCount = 0;

    void add(Product product) {
        if (productCount < 10) {
            products[productCount] = product;
            productCount++;
        }
    }


    double totalPrice() {
        double totalPrice = 0;
        for (int i = 0; i < productCount; i++) {
            totalPrice += products[i].getPrize();

        }
        return totalPrice;
    }
}
