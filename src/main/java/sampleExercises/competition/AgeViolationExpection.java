package sampleExercises.competition;



public class AgeViolationExpection extends RuntimeException{

    private final int ageRequired;

    public AgeViolationExpection(int ageRequired){
        this.ageRequired=ageRequired;
    }

    public int getAgeRequired() {
        return ageRequired;
    }
}
