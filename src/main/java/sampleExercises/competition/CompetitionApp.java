package sampleExercises.competition;

public class CompetitionApp {
    public static void main(String[] args) {

        CompetitionController competitionController = new CompetitionController();
        competitionController.run();
    }
}
