package sampleExercises.competition;

public class DuplicateExceptionParticipant extends RuntimeException {
   private  Participant participant;

    public DuplicateExceptionParticipant(Participant participant) {
        this.participant = participant;
    }

    public Participant getParticipant() {
        return participant;
    }
}
