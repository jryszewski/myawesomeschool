package sampleExercises.competition;


public class MaxCompetitiorsException extends RuntimeException {

    private final int maxCompetitors;


    public MaxCompetitiorsException(int maxCompetitors) {
        this.maxCompetitors = maxCompetitors;
    }

    public int getMaxCompetitors() {
        return maxCompetitors;
    }
}
