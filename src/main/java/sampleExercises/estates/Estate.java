package sampleExercises.estates;

public class Estate implements Comparable<Estate> {

    private String city;
    private double price;
    private double area;

    public Estate(String city, double price, double area) {
        this.city = city;
        this.price = price;
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    double getPricePerSqm(){
        return price/area;
    }

    @Override
    public int compareTo(Estate estate) {
        return Double.compare(getPricePerSqm(),estate.getPricePerSqm());
    }

    @Override
    public String toString() {
        return city + ", " + price + "zł, " +area +"mkw, "+ String.format("%.2f",getPricePerSqm() ) +"zł/mkw";
    }
}
