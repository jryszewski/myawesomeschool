package sampleExercises.estates;


import java.io.*;


public class EstateReader {


     static Estate[] readFile(String fileName) {
        Estate[] estates = new Estate[10];
        int i = 0;
        try (
                FileReader fileReader = new FileReader(fileName);
                BufferedReader reader = new BufferedReader(fileReader);
        ) {
            String nextLine = null;
            while ((nextLine = reader.readLine()) != null) {
                estates[i] = saveOffer(nextLine);
                i++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return estates;
    }

    private static Estate saveOffer(String nextLine) {

        String[] data = nextLine.split(";");
        return new Estate(data[0], Double.parseDouble(data[1]), Double.parseDouble(data[2]));
    }
}
