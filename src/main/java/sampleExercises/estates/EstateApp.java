package sampleExercises.estates;

import java.util.Arrays;

public class EstateApp {
    public static void main(String[] args) {
          final String fileName = "C:\\Users\\jrysz\\Desktop\\estates.txt";
        Estate[] estates = EstateReader.readFile(fileName);
        Arrays.sort(estates);
        for(Estate estate: estates){
            System.out.println(estate);
        }

    }
}
