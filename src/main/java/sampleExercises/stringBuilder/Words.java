package sampleExercises.stringBuilder;

import java.util.Scanner;

public class Words {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbe wyrazów: ");
        int number = scanner.nextInt();
        scanner.nextLine();
        String[] words = new String[number];
        for (int i=0; i<number; i++){
            words[i] = scanner.nextLine();

        }

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i <number ; i++) {
            char znak = words[i].charAt(words[i].length()-1);
            builder.append(znak);
        }

        String wynik = builder.toString();
        System.out.println(wynik);










    }
}
