package sampleExercises.stringBuilder;

import java.util.Scanner;

public class Changer {

    public static void main(String[] args) {

        System.out.println("Podaj dowolny napis");

        Scanner input = new Scanner(System.in);
        String words = input.nextLine();

         char znak = words.charAt(0);

        if(Character.isUpperCase(znak)){
            String wynik = words.toUpperCase();
            System.out.println(wynik);
        }  else if(Character.isLowerCase(znak)) {
            String wynik2 = words.toLowerCase();
            System.out.println(wynik2);
        } else {
            System.out.println(words);
        }
    }


}
