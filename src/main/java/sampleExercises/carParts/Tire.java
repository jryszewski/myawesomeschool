package sampleExercises.carParts;

public class Tire extends Part {
    private String size;
    private int width;

    public Tire(int id, String producentName, String model, String seria, String size, int width) {
        super(id, producentName, model, seria);
        this.size = size;
        this.width = width;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
