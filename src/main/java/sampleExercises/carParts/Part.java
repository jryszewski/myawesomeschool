package sampleExercises.carParts;

public class Part {

    private int id;
    private String producentName;
    private String model;
    private String seria;

    public Part(int id, String producentName, String model, String seria) {
        this.id = id;
        this.producentName = producentName;
        this.model = model;
        this.seria = seria;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProducentName() {
        return producentName;
    }

    public void setProducentName(String producentName) {
        this.producentName = producentName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSeria() {
        return seria;
    }

    public void setSeria(String seria) {
        this.seria = seria;
    }
}
