package sampleExercises.pizzeria;

import java.util.Scanner;

public class Pizzeria {

    public static void main(String[] args) {

        System.out.println("Dostępne pizze: ");

        for (Pizza p:Pizza.values()
             ) {
            System.out.println(p);

        }

        Scanner scanner = new Scanner(System.in);
        System.out.println("wybierz pizze: ");
        String choose = scanner.nextLine();
        Pizza pizza = Pizza.valueOf(choose.toUpperCase());

        System.out.println("Dziękujemy za zamówienie pizzy: " + pizza.name());
        scanner.close();



    }
}
