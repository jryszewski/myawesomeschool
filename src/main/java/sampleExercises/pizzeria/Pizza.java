package sampleExercises.pizzeria;

public enum Pizza {
    MARGHERITA(true, true, false, false),
    CAPRICIOSA(true, true, true, false),
    PROSCIUTTO(true, true, false, true);


    private boolean sosPomidorowy;
    private boolean ser;
    private boolean pieczarki;
    private boolean szynka;

    Pizza(boolean sosPomidorowy, boolean ser, boolean pieczarki, boolean szynka) {
        this.sosPomidorowy = sosPomidorowy;
        this.ser = ser;
        this.pieczarki = pieczarki;
        this.szynka = szynka;
    }

    public boolean isSosPomidorowy() {
        return sosPomidorowy;
    }

    public void setSosPomidorowy(boolean sosPomidorowy) {
        this.sosPomidorowy = sosPomidorowy;
    }

    public boolean isSer() {
        return ser;
    }

    public void setSer(boolean ser) {
        this.ser = ser;
    }

    public boolean isPieczarki() {
        return pieczarki;
    }

    public void setPieczarki(boolean pieczarki) {
        this.pieczarki = pieczarki;
    }

    public boolean isSzynka() {
        return szynka;
    }

    public void setSzynka(boolean szynka) {
        this.szynka = szynka;
    }

    @Override
    public String toString() {
        String result = name() + "(";

        if (sosPomidorowy) {
            result += "sos pomidorowy";
        }
        if (ser) {
            result += ", ser";
        }
        if (pieczarki) {
            result += ", pieczarki";
        }
        if (szynka) {
            result += ", szynka";
        }
        result += ")";
        return result;
    }
}
