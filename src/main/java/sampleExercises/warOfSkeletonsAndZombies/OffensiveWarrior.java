package sampleExercises.warOfSkeletonsAndZombies;

public class OffensiveWarrior extends Warriors {

    private double additionalAttackPerPercent;

    public OffensiveWarrior(int attackPoints, int deffPoints, int lifePoints, double additionalAttackPerPercent) {
        super(attackPoints, deffPoints, lifePoints);
        this.additionalAttackPerPercent = additionalAttackPerPercent;
    }

    public double getAdditionalAttackPerPercent() {
        return additionalAttackPerPercent;
    }

    public void setAdditionalAttackPerPercent(double additionalAttackPerPercent) {
        this.additionalAttackPerPercent = additionalAttackPerPercent;
    }

    @Override
    double totalAtak() {
        return getAttackPoints()+getAdditionalAttackPerPercent()*getAttackPoints();
    }
}
