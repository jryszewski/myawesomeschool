package sampleExercises.warOfSkeletonsAndZombies;

public class Warriors {

    private int attackPoints;
    private int deffPoints;
    private int lifePoints;

    public Warriors(int attackPoints, int deffPoints, int lifePoints) {
        this.attackPoints = attackPoints;
        this.deffPoints = deffPoints;
        this.lifePoints = lifePoints;
    }

    public int getAttackPoints() {
        return attackPoints;
    }

    public void setAttackPoints(int attackPoints) {
        this.attackPoints = attackPoints;
    }

    public int getDeffPoints() {
        return deffPoints;
    }

    public void setDeffPoints(int deffPoints) {
        this.deffPoints = deffPoints;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    double totalAtak(){
        return getAttackPoints();
    }

    double totalDefense(){
        return getDeffPoints();
    }
}
