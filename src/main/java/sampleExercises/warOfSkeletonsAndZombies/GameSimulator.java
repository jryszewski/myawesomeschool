package sampleExercises.warOfSkeletonsAndZombies;

public class GameSimulator {

    public static void main(String[] args) {

        OffensiveWarrior offensiveWarrior = new OffensiveWarrior(100,100,100,0.20);
        OffensiveWarrior offensiveWarrior1 = new OffensiveWarrior(100,100,100,0.20);
        OffensiveWarrior offensiveWarrior2 = new OffensiveWarrior(150,100,100,0.20);
        OffensiveWarrior offensiveWarrior3 = new OffensiveWarrior(100,100,100,0.20);
        OffensiveWarrior offensiveWarrior4 = new OffensiveWarrior(100,100,100,0.20);
        OffensiveWarrior offensiveWarrior5 = new OffensiveWarrior(100,100,100,0.20);


    Team team1 = new Team("TeamOne",offensiveWarrior1, offensiveWarrior, offensiveWarrior3);
    Team team2 = new Team("TeamTwo",offensiveWarrior4, offensiveWarrior2, offensiveWarrior5);
    WarriorsBattleApp.fight(team1, team2);
}}
