package sampleExercises.warOfSkeletonsAndZombies;

public class Team {

   private  String name;
   private Warriors[] warriors = new Warriors[3];

    public Team(String name, Warriors warriors1, Warriors warriors2, Warriors warriors3){

        this.name = name;
        this.warriors[0]=warriors1;
        this.warriors[1]=warriors2;
        this.warriors[2]=warriors3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Warriors[] getWarriors() {
        return warriors;
    }

    public void setWarriors(Warriors[] warriors) {
        this.warriors = warriors;
    }

    double attack(){
        double attack = 0;
        for (Warriors warriors:warriors
             ) {
            attack+=warriors.totalAtak();
        }
        return attack;
    }

    double deffence(){
        double deffence=0;
        for (Warriors warriors: warriors
             ) {
            deffence+=warriors.totalDefense();
        }
        return deffence;
    }
    double energy(){
        double energy=0;
        for (Warriors warriors: warriors
             ) {
            energy = warriors.getLifePoints();
        }
        return energy;
    }
}
