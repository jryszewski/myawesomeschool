package sampleExercises.warOfSkeletonsAndZombies;

public class DeffensiveWarriors extends Warriors {



    private double deffBonus;

    public DeffensiveWarriors(int attackPoints, int deffPoints, int lifePoints, double deffBonus) {
        super(attackPoints, deffPoints, lifePoints);
        this.deffBonus = deffBonus;
    }

    public double getDeffBonus() {
        return deffBonus;
    }

    public void setDeffBonus(double deffBonus) {
        this.deffBonus = deffBonus;
    }

    @Override
    double totalDefense() {
        return getDeffBonus()*getDeffPoints()+getDeffPoints();
    }
}
