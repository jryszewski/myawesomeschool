package sampleExercises.hospital;

public class Nurse extends  Person{

    private int overtime;



    public int getOvertime() {
        return overtime;
    }

    public void setOvertime(int overtime) {
        this.overtime = overtime;
    }

    public Nurse(String name, String lastName, int payoutAmount, int overtime) {
        super(name, lastName, payoutAmount);
        this.overtime = overtime;
    }

    @Override
    public String getInfo() {
        return super.getInfo()+ ", Nadgodziny: "+ overtime;
    }
}
