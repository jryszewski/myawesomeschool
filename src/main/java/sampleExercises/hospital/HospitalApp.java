package sampleExercises.hospital;

public class HospitalApp {

    public static void main(String[] args) {

        Hospital hospital = new Hospital();
        hospital.add(new Doctor("Maciek", "Prefeska", 10000, 20000));
        hospital.add(new Nurse("Wisława", "Wiskitek", 1000, 5));
        hospital.add(new Person("jacek", "Stonoga", 100));
        hospital.add(new Doctor("zyd", "konopny", 44444, 444));

        System.out.println(hospital.getInfo());
    }



}
