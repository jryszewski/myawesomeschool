package sampleExercises.hospital;

public class Hospital {
    private static final int maxEmployees = 3;
    Person[] employees = new Person[maxEmployees];
    private int employeesNumber = 0;


    public void add(Person person) {
        if (employeesNumber < maxEmployees) {
            employees[employeesNumber] = person;
            employeesNumber++;
        }


    }

    String getInfo() {

        String result = "";

        for (int i = 0; i < employeesNumber; i++) {
            result = result + employees[i].getInfo() + "\n";
        }
        return result;
    }

}
