package sampleExercises.hospital;

public class Doctor extends Person{


    public Doctor(String name, String lastName, int payoutAmount, int bonus) {
        super(name, lastName, payoutAmount);
        this.bonus = bonus;
    }

    private int bonus;

    public int getBonus() {
        return bonus;
    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    @Override
    public String getInfo() {
        return super.getInfo()+ ", Premia: "+ bonus;
    }
}
