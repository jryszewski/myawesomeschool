package sampleExercises.hospital;

public class Person {

    private String name;
    private String lastName;
    private int payoutAmount;

    public Person(String name, String lastName, int payoutAmount) {
        this.name = name;
        this.lastName = lastName;
        this.payoutAmount = payoutAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getPayoutAmount() {
        return payoutAmount;
    }

    public void setPayoutAmount(int payoutAmount) {
        this.payoutAmount = payoutAmount;
    }

    public String getInfo() {
        return "Imię: " + name + ", Nazwisko: " + lastName + ", Wypłata " + payoutAmount;
    }
}
