package sampleExercises.sortBubbles;


import java.util.Arrays;

public class BubbleSort {

    public static void main(String[] args) {

        int[] tab = {5, 38, 88, 107, 3, 1, 20};


        for (int i = 0; i < tab.length; i++) {
            for (int j = 1; j < tab.length -i; j++) {

                if (tab[j-1] > tab[j]) {
                    int max = tab[j];
                    tab[j] = tab[j - 1];
                    tab[j - 1] = max;
                }
            }

        }
        System.out.println();
        for (int i = 0; i < tab.length; i++) {
            System.out.println((tab[i] + " "));
        }
    }
}
