package sampleExercises.internetShop;

import sampleExercises.firmSaveReadFromFile.Company;

import javax.persistence.criteria.CriteriaBuilder;
import javax.xml.crypto.Data;
import java.io.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ShopApp {

    private static final String fileName = "C:\\Users\\jrysz\\Desktop\\clients.csv";
    private static Scanner scanner = new Scanner(System.in);
    private static final int read_file = 1;
    private static final int findBestClient = 2;
    private static final int clientInfo = 3;
    private static final int exit = 4;

    public static void main(String[] args) throws FileNotFoundException {


        Customer[] customers = DataReader.readFile(fileName);

       int option = 0;
        do {
            printInfo();
            try{
                option = scanner.nextInt();
            }catch (InputMismatchException e){
                System.out.println("Musisz wpisać liczbę gamoniu jeden!");
                scanner.nextLine();
            }

            
            switch (option) {
                case read_file:

                    printAllCustomers(customers);

                    break;
                case findBestClient:

                    printMostValuableClient(customers);
                    break;
                case clientInfo:

                    String country = readCountryFromUser();
                    printCustomersFromCountry(customers, country);
                    printAvgValueForCountry(customers, country);
                    break;
                case exit:
                    scanner.close();
                    break;
                default:
                    System.out.println("nie ma takiej opcji spróbuj ponownie");
                   

            }
        } while (option != exit);
    }
    static void printInfo(){
        System.out.println("");
        System.out.println("Wczytanie bazy klientów -" + read_file);
        System.out.println("Znajdz klienta z najwiekszym portfelem -" + findBestClient);
        System.out.println("Wyswietl informacje o klientach z wybranego kraju -" + clientInfo);
        System.out.println("Zakończ - " + exit);
    }
    private static void printAvgValueForCountry(Customer[] customers, String country) {
        double suma=0;
        int licznik=0;
        for(Customer customer: customers){
            if(customer.getCountry().toLowerCase().equals(country.toLowerCase())){
                suma+=customer.getWallet();
                licznik++;
            }
        }
        double wynik = suma/licznik;
        if(suma>0) {
            System.out.printf("Oto średni portfel klienta to: %.2f%n  ",wynik);
            System.out.println("Liczba klientów z "+ country+ " wynosi: " + licznik);
        }
    }

    private static void printCustomersFromCountry(Customer[] customers, String country){
        System.out.println("Kliencji z kraju " + country+":");
        boolean atLeastOneClient = false;
        for(Customer customer: customers){
            if(customer.getCountry().toLowerCase().equals(country.toLowerCase())){
                System.out.println(customer);
                atLeastOneClient=true;
            }

        }
        if(!atLeastOneClient){
            System.out.println("brak klientów");
        }
    }

    private static String readCountryFromUser() {

        System.out.println("Podaj państwo: ");
        scanner.nextLine();
        return scanner.nextLine();
    }

    private static void printMostValuableClient(Customer[] customers) {
        Customer mostValuale = customers[0];
        for (Customer custom : customers
        ) {
            if (custom.getWallet() > mostValuale.getWallet()) {
                mostValuale = custom;
            }

        }
        System.out.println("najbardzier wartosciowy klient to: " + mostValuale);

    }


    public static void printAllCustomers(Customer[] customers) {

        for (Customer custome : customers
        ) {
            System.out.println(custome);
        }

    }
}



