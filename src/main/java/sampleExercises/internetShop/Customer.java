package sampleExercises.internetShop;

import java.io.Serializable;

public class Customer implements Serializable {

    private int id;
    private String firstName;
    private String lastName;
    private String country;
    private double wallet;

    public Customer(int id, String firstName, String lastName, String country, double wallet) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.wallet = wallet;
    }

    public float getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getWallet() {
        return wallet;
    }

    public void setWallet(double wallet) {
        this.wallet = wallet;
    }

    @Override
    public String toString() {
        return id + ": " + firstName + " " + lastName+", "+country+", "+ wallet;
    }
}
