package sampleExercises.internetShop;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class DataReader {
     static Customer[] readFile(String filename) throws FileNotFoundException {
        final int linesNumber = countLinesWithoutHeader(filename);
        Customer[] customers = new Customer[linesNumber];
        try(Scanner scanner = new Scanner(new File(filename))){
            scanner.nextLine();// pomijamy naglowek
            for (int i = 0; i <linesNumber ; i++) {
                String line = scanner.nextLine();
                customers[i]=createClientFromTxt(line);
            }
        }
        return customers;
    }

     static Customer createClientFromTxt(String line) {

        String[] data = line.split(",");
        int id = Integer.parseInt(data[0]);
        String firstName = data[1];
        String lastname = data[2];
        String country = data[3];
        double value= Double.parseDouble(data[4]);
        return new Customer(id,firstName,lastname,country,value);
    }

     static int countLinesWithoutHeader(String filename) throws FileNotFoundException {

        int lines=0;
        try(Scanner scanner = new Scanner(new File(filename))){
            scanner.nextLine();
            while(scanner.hasNextLine()){
                scanner.nextLine();
                lines++;
            }
        }
        return lines;
    }

}
