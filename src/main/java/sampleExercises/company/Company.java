package sampleExercises.company;

public class Company {

    public static void main(String[] args) {

        Employee[] employees = createEmployees();
        printEmployees(employees);
        System.out.println("Suma miesiecznych wynagrodzeń: " + totalMonthlySalaries(employees));
        System.out.println("Suma rocznych wynagrodzeń: " + totalYearlySalaries(employees));

    }

    private static Employee[] createEmployees() {
        Employee[] employees = new Employee[2];
        employees[0] = new FullTimeWorker("Ania", "Konar", 5000);
        employees[1] = new PartTimeWorker("Felicja", "Skoda", 20, 38);
        return employees;
    }



    private static void printEmployees(Employee[] employees){
        System.out.println("Pracownicy: ");
        for (Employee emp: employees) {
            System.out.println(emp);
        }
    }

    private static double totalYearlySalaries(Employee[] employees){

        double totalYearlySalaries=0;
        for (Employee emp:employees
             ) {
            totalYearlySalaries+=emp.annualPayment();

        }
        return totalYearlySalaries;
    }

    private static double totalMonthlySalaries(Employee[] employees){
        double totalMonthySalaries = 0;

        for (Employee emp: employees
             ) {

            totalMonthySalaries+=emp.monthlyPayment();

        }
        return totalMonthySalaries;
    }
}
