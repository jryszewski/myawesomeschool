package sampleExercises.company;

public class FullTimeWorker extends Employee {

    private double monthIncomes = 5000;

    public FullTimeWorker(String name, String surname, double monthIncomes) {
        super(name, surname);
        this.monthIncomes = monthIncomes;
    }

    public double getMonthIncomes() {
        return monthIncomes;
    }

    public void setMonthIncomes(double monthIncomes) {
        this.monthIncomes = monthIncomes;
    }

    @Override
    public double monthlyPayment() {
        return monthIncomes;
    }

    @Override
    public double annualPayment() {
        double bonus = 1.05;
        double monthInYear = 12;
        return (monthIncomes* monthInYear)* bonus;
    }
}
