package sampleExercises.company;

public class PartTimeWorker extends Employee {

    private double hourlyRate = 80;
    private int numberOfHours = 20;

    public PartTimeWorker(String name, String surname, double hourlyRate, int numberOfHours) {
        super(name, surname);
        this.hourlyRate = hourlyRate;
        this.numberOfHours = numberOfHours;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public int getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(int numberOfHours) {
        this.numberOfHours = numberOfHours;
    }


    @Override
    public double monthlyPayment() {
        return hourlyRate*numberOfHours;
    }

    @Override
    public double annualPayment() {
        int monthsInYear = 12;
        return hourlyRate*numberOfHours* monthsInYear;
    }
}
