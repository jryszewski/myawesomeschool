package sampleExercises.calculatorWithInterface;

import library.exception.NoSuchOptionException;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;

public class Main {

    public static void main(String[] args) {
        ShapeCalculator shapeCalc = new ShapeCalculator();
        Shape shape = null;

        boolean readComplete = false;

        while(!readComplete){
            try{
                printOptions();
                shape = shapeCalc.createShape();
                readComplete = true;
            }catch (InputMismatchException e){
                System.out.println("wprowadziles dane w zływ formacie");
            }

            catch (NoSuchElementException e){
                System.out.println("Wybrany identyfikator kształtu jest niepoprawny, sprobuj ponownie");
            }finally {
                shapeCalc.clearBuffer();
            }
        }
        System.out.println(shape);
        shapeCalc.closeScanner();
    }

    private static void printOptions() {
        System.out.println("Wybierz figure, dla ktorej chcesz obliczyc pole: ");
        System.out.println(Shape.RECTANGLE + " - prostokąt");
        System.out.println(Shape.CIRCLE + " - koło");
        System.out.println(Shape.TRIANGLE + " - trójkąt");
    }
}
