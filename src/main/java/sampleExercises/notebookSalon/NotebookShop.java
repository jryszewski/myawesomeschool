package sampleExercises.notebookSalon;

import java.util.Arrays;

public class NotebookShop {

    public static void main(String[] args) {

        DataStore dataStore = new DataStore();
       dataStore.add(new Computer("Lenovo",112));
       dataStore.add(new Computer("Hasus",2));
       dataStore.add(new Computer("Asus",322));
       dataStore.add(new Computer("Dupeus",4121));
       dataStore.add(new Computer("Lenovo",112));
        Computer computer = new Computer("Hasus",2);
        System.out.println(Arrays.toString(dataStore.actualNotebooksInfo()));
        System.out.println(dataStore.checkAvailability(computer));


    }


}
