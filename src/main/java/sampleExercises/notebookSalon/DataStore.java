package sampleExercises.notebookSalon;

public class DataStore {

    private final int maximumComputers = 100;
    Computer [] computers = new Computer[maximumComputers];
    private int computersNumber=0;

    public void add(Computer computer){
       if(computersNumber<maximumComputers){
            computers[computersNumber]= computer;
            computersNumber++;
        }
    }

    public Computer[] actualNotebooksInfo(){

        Computer[] comps = new Computer[computersNumber];
        for (int i=0;i<computersNumber;i++){
            comps[i]=computers[i];
        }
       return comps;
    }

    public int checkAvailability(Computer computer){
         int count=0;
        for (Computer computer1: computers
        ) {
           if( computer.equals(computer1)){
               count++;
           }



        }
        return count;
    }

}
