package sampleExercises;

public class CartTest {

    public static void main(String[] args) {

        Cart cart = new Cart();
        cart.add(new Product("mleko",25));
        cart.add(new Product("jaja",5));
        cart.add(new Product("piza",3));

        double cartCost = cart.totalPrice();
        System.out.println("koszt koszyka: " +cartCost + "zł");
    }
}
