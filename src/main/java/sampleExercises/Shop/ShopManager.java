package sampleExercises.Shop;

import java.util.Scanner;

public class ShopManager {

    public static void main(String[] args) {

        Order[] orders = createOrders();
        Status status = readStatus();
        Order[] filteredOrders = filterOrdersByStatus(orders,status);
        printOrders(filteredOrders,status);


    }

        private static Status readStatus(){
            Scanner sc = new Scanner(System.in);
            System.out.println("Proszę podać status zamówienia: ");
            for (Status st : Status.values()
            ) {
                System.out.println(st.name() + "");

            }
            System.out.println("):");
            String choose = sc.nextLine();
            return Status.valueOf(choose);
        }



    public static Order[] filterOrdersByStatus(Order[] orders, Status status){
        int size = countOrderByStatus(orders, status);
        Order[] ordersByStatus = new Order[size];
        int counter=0;
        for (Order order: orders
             ) {
            if(order.getStatus()==status){
                ordersByStatus[counter]= order;
                counter++;
            }
        }

        return ordersByStatus;
    }

    private static int countOrderByStatus(Order[] orders, Status status){
        int count = 0;
        for(Order order: orders){
            if(order.getStatus()==status){
                count++;
            }
        }
        return count;
    }

    public static void printOrders(Order[] filteredOrders, Status status){
        System.out.println("Lista zamówień ze statusem: "+ status);
        for (Order order : filteredOrders
             ) {
            System.out.println(order);

        }
    }

    private static Order[] createOrders() {
        Order[] orders = new Order[10];
        orders[0] = new Order("Komputer Apple MacBook Pro 2018", 4999, Status.PAID);
        orders[1] = new Order("Telefon Samsung Galaxy s10", 2499, Status.SHIPPED);
        orders[2] = new Order("Telefon Apple iPhone X", 2999, Status.PAID);
        orders[3] = new Order("Pralka Beko X2000", 1499, Status.DELIVERED);
        orders[4] = new Order("Lodówka Samsung AT200", 1199, Status.DELIVERED);
        orders[5] = new Order("Głośnik JBL z100", 199, Status.NEW);
        orders[6] = new Order("Kino Domowe Yamaha XBC900", 3499, Status.CANCELLED);
        orders[7] = new Order("Konsola Sony Playstation 4", 1499, Status.SHIPPED);
        orders[8] = new Order("Adapter USBC - HDMI", 149, Status.PAID);
        orders[9] = new Order("Żelazko Zelmer PRIMO", 99.99, Status.SHIPPED);
        return orders;
    }

}
