package sampleExercises.Shop;

public enum Status {

    NEW("złożono zamówienie, ale jeszcze go nie opłacono"),
    PAID("zamówienie opłacono"),
    SHIPPED("zamówienie wysłane"),
    DELIVERED("zamówienie dostarczone"),
    CANCELLED("zamówienie anulowano");

    private String description;

    Status(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }


}
