package sampleExercises;

public class Silnia {
    public static void main(String[] args) {

        long wynik = silnia(15);
        System.out.println(wynik);
    }

    public static long silnia(long a){
        long result=1;
        while(a>0){
            result = result*a;
            a--;
        }
        return  result;
    }
}
