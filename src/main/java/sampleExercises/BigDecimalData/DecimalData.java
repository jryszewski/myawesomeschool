package sampleExercises.BigDecimalData;

import java.io.*;
import java.math.BigInteger;
import java.util.Scanner;

public class DecimalData {
    public static void main(String[] args) throws IOException {


        final String FILE_NAME = "C:\\Users\\jrysz\\Desktop\\numbers.txt";

        FileReader fileReader = new FileReader(FILE_NAME);
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        String text = bufferedReader.readLine();
        System.out.println(text);
        int digits = CountNumbers(text);
        System.out.println(digits);
        int letters = CountChars(text);
        System.out.println(letters);
        int biggestNumber= BiggestNuber(text);
        System.out.println(biggestNumber);

    }

        static int CountNumbers(String text){

            char[] chars = text.toCharArray();
            int digits= 0;
            for (char aChar: chars){
                if(Character.isDigit(aChar)){
                    digits++;
                }
            }
           return digits;
        }

        static int BiggestNuber(String text){
            char[] chars = text.toCharArray();
            int biggestNumber = -1;
            for(char aChar: chars){
                if (Character.isDigit(aChar)) {

                    int digit = Character.getNumericValue(aChar);
                    biggestNumber = Integer.max(digit,biggestNumber);
                }

            }
            return biggestNumber;
        }

        static int CountChars(String text){
        char[] chars = text.toCharArray();
        int letters= 0;
        for(char aChar: chars){
            if(Character.isLetter(aChar)){
                letters++;
            }
        }
        return letters;
        }
      /*  File file = new File(FILE_NAME);

        Scanner scanner = new Scanner(file);

        int a = Integer.parseInt(scanner.nextLine());
        int b = Integer.parseInt(scanner.nextLine());
        int c = Integer.parseInt(scanner.nextLine());
        BigInteger d = new BigInteger(scanner.nextLine());
        BigInteger e = new BigInteger(scanner.nextLine());
        System.out.println(a + b + c);
        System.out.println(d.add(e));


        scanner.close();*/



}

