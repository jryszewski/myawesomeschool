package sampleExercises.calculator;

public class Operation {

    public void calculate(double a, double b, String sign) throws UnknownOperatorException {
      switch (sign){
          case"+":
              sum(a,b);
              break;
          case"-":
              substract(a,b);
              break;
          case"/":
              if (b==0)
                  throw new ArithmeticException("nie mozesz dzielic przez zero");
              divide(a,b);
              break;
          case"*":
              multiply(a,b);
              break;
          default:
              throw new UnknownOperatorException("Błędny operator matematyczny");
      }
    }
    public void sum(double a, double b){
        double result = 0;
        result=a+b;
        System.out.println(result);

    }
    public void substract(double a, double b){
        double result = 0;
        result=a-b;
        System.out.println(result);

    }
    public void divide(double a, double b) throws ArithmeticException{
        double result = 0;
        result=a/b;
        System.out.println(result);

    }
    public void multiply(double a, double b){
        double result = 0;
        result=a*b;
        System.out.println(result);

    }
}
