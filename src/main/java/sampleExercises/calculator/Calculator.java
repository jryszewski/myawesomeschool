package sampleExercises.calculator;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) throws UnknownOperatorException {
        Scanner sc = new Scanner(System.in);
        double number1 = 0;
        double number2 = 0;
        String sign=null;
        boolean error = true;

        do{
            try{
                System.out.println("Podaj 1 liczbę: ");
                number1 = sc.nextDouble();
                System.out.println("Podaj 2 liczbę: ");
                number2 = sc.nextDouble();

                System.out.println("Jaką operację chcesz wykonać, wybierz operator matematyczny: (+, -, *, /) ?");
                sc.nextLine();
                sign = sc.nextLine();
                error= false;
                Operation operation = new Operation();
                operation.calculate(number1, number2, sign);
            } catch ( InputMismatchException e){
                System.out.println("Błędna wartość, wpisz liczbę");
                sc.nextLine();
            }catch (UnknownOperatorException | ArithmeticException e){
                System.out.println(e.getMessage());
                System.out.println("nie udalo sie obliczyc wyniku wyrazenia");
            }
        } while(error);
        sc.close();






    }






}
