package sampleExercises.calculator;


public class UnknownOperatorException extends Exception {
    public UnknownOperatorException(String message) {
        super(message);
    }
}
